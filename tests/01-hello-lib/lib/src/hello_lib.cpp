#include <iostream>
#include "hello_lib.hpp"

bool greetings(std::string const& user_name) {
  using namespace std;

  std::string output_phrase("Hello, ");

  cout << output_phrase << user_name << endl;

  return cout.good();
}
